package main

import (
	"net/http"
	"github.com/gin-gonic/gin"
	_"fmt"
	"log"
	"io/ioutil"
	"encoding/json"
	_"sync"
)



func setupRouter() *gin.Engine {
	r := gin.Default()

	// Ping test
	r.GET("/", func(c *gin.Context) {
		resp := getResponseFromMicro()
		log.Println(resp)
		c.JSON(http.StatusOK, gin.H{"text": resp.RestText})
	})

	return r
}

func main() {
	r := setupRouter()
	r.Run(":8082")
}

type Resp struct {
	RestText string `json:"text"`
}

func getResponseFromMicro()  Resp {
	response, err := http.Get("http://restservice:8083/ping")
	if err != nil {
		log.Println(err.Error())
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println(err.Error())
	}
	log.Println(body)
	resp := Resp{}
	json.Unmarshal(body, &resp)
	log.Println(resp)
	for i := 0; i < 1000; i++ {
		go func() {
			_, e := http.Get("http://restservice:8083/ping")
			if e != nil {
				log.Println(e.Error())
			}
		}();
	}
	return resp
}
